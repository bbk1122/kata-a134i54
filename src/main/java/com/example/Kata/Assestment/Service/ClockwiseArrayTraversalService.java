package com.example.Kata.Assestment.Service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.example.Kata.Assestment.ServiceImpl.ClockwiseArrayTraversalServiceImpl;

@Service
public class ClockwiseArrayTraversalService {

	
	//ClockwiseArrayTraversalServiceImpl clockwiseArrayTraversalServiceImpl;

	public ResponseEntity<?> clockwiseTraversal(String inputJson) {
	return ClockwiseArrayTraversalServiceImpl.clockwiseTraversal(inputJson);
	}

}
