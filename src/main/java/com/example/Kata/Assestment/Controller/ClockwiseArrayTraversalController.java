package com.example.Kata.Assestment.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.Kata.Assestment.Service.ClockwiseArrayTraversalService;

@Controller
public class ClockwiseArrayTraversalController {

	@Autowired
	ClockwiseArrayTraversalService clockwiseArrayTraversalService;

	@GetMapping("/traverse")
	public ResponseEntity<?> clockwiseTraversal(@RequestBody String inputJson) {
		return clockwiseArrayTraversalService.clockwiseTraversal(inputJson);
	}

}
