package com.example.Kata.Assestment.ServiceImpl;

import java.util.Arrays;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.example.Kata.Assestment.Traversal.ClockwiseArrayTraversal;
import com.google.gson.Gson;

public class ClockwiseArrayTraversalServiceImpl {
	static int[][] inputArray;

	public static ResponseEntity<?> clockwiseTraversal(String inputJSON) {
		System.out.println("Hits");
		inputArray = parseJsonInput(inputJSON);
		if(inputArray.length>2)
		{
			List<Integer> result = ClockwiseArrayTraversal.clockwiseArrayTraversal(inputArray);
			return new ResponseEntity<>(result.toString(), HttpStatus.OK);
		}else
		{
			return new ResponseEntity<>("Please Try with Valid Data", HttpStatus.NO_CONTENT);
		}
	}

	public static int[][] parseJsonInput(String Json) {
		Gson gson = new Gson();
		System.out.println(Arrays.toString(inputArray));
		return gson.fromJson(Json, int[][].class);
	}
}
