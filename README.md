# Kata A134i54

```
Basic Git Commands : 
git clone https://gitlab.com/bbk1122/kata-a134i54.git
cd existing_repo
git remote add origin https://gitlab.com/bbk1122/kata-a134i54.git
git branch -M main
git push -uf origin main
```

```
How to Run the Application

1. Checkout the repo to your local
2. import the project in IDE
3. Run the SpringBoot Application
4. Test the Application using
	4.1 URL : http://localhost:8080/traverse, 
	4.2 Request Body : raw -> 
	[
    [
        1,
        2,
        3,
        4
    ],
    [
        5,
        6,
        7,
        8
    ],
    [
        9,
        10,
        11,
        12
    ]
	]

	4.3 Sample Output : [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7]


```

```
TechStack :
SpringBoot
Maven
Java8
```